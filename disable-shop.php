<?php
/*
 * Plugin Name:  Disable Shop
 * Plugin URI:   https://bitbucket.org/ngearing/disable-shop
 * Description:  Disable users from accessing the cart and checkout of your shop.
 * Version:      0.0.4.1
 * Author:       Nathan
 * Author URI:   https://greengraphics.com.au
 * License:      GPL2
 * License URI:  https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain:  da
 * Bitbucket Plugin URI: https://bitbucket.org/ngearing/disable-shop
 */

/**
 * Setup function
 *
 * @return void
 */
function da_setup() {
	if ( ! current_user_can( 'administrator' ) ) {
		remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30 );
		add_action( 'template_redirect', 'da_shop_redirect' );
		add_filter( 'woocommerce_loop_add_to_cart_link', 'da_add_to_cart_link', 10, 3 );
	}
}
add_action( 'plugins_loaded', 'da_setup' );

function da_conditions() {
	return ( ! current_user_can( 'administrator' ) && ( is_cart() || is_checkout() ) ) ? true : false;
}

/**
 * Change add to cart button on archive
 *
 * @param [type] $output
 * @param [type] $product
 * @param [type] $args
 * @return void
 */
function da_add_to_cart_link( $output, $product, $args ) {
	if ( $product->is_purchasable() && $product->is_in_stock() ) {
		$output = sprintf(
			'<a href="%s" class="%s" %s>%s</a>',
			esc_url( $product->get_permalink() ),
			esc_attr( 'button product_type_simple' ),
			isset( $args['attributes'] ) ? wc_implode_html_attributes( $args['attributes'] ) : '',
			esc_html( 'Read More' )
		);
	}

	return $output;
}

/**
 * Shop redirect
 *
 * @return void
 */
function da_shop_redirect() {
	if ( ! da_conditions() ) {
		return;
	}

	wp_safe_redirect( home_url( 'shop', is_ssl() ), '307' );
	exit;
}
